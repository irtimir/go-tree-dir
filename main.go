package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
)

func drawTree(out io.Writer, path, prefix string, printFiles bool) {
	elements, _ := ioutil.ReadDir(path)

	sort.Slice(elements, func(i, j int) bool {
		return elements[i].Name() < elements[j].Name()
	})

	var lengthElements int

	if printFiles {
		lengthElements = len(elements)
	} else {
		for _, elem := range elements {
			if elem.IsDir() {
				lengthElements++
			}
		}
	}

	var numOfElement int
	var elemPrefix string
	var preElemPrefix string

	for _, elem := range elements {
		if numOfElement == lengthElements-1 {
			elemPrefix = "└───"
			preElemPrefix = prefix + "\t"
		} else {
			elemPrefix = "├───"
			preElemPrefix = prefix + "│\t"
		}

		if printFiles && !elem.IsDir() {
			fSize := "empty"
			if elem.Size() != 0 {
				fSize = strconv.FormatInt(elem.Size(), 10) + "b"
			}
			fmt.Fprintf(out, "%v%v%v (%v)\n", prefix, elemPrefix, elem.Name(), fSize)
			numOfElement++
		} else if elem.IsDir() {
			fmt.Fprintf(out, "%v%v%v\n", prefix, elemPrefix, elem.Name())
			numOfElement++
			drawTree(out, path+"/"+elem.Name(), preElemPrefix, printFiles)
		}
	}
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	drawTree(out, path, "", printFiles)
	return nil
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
